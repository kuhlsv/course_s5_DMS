﻿using DMS_Client.Database;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace DMS_Client.Logs
{
    /// <summary>
    /// Logger singleton object
    /// LogType can be changed
    /// </summary>
    class Logger
    {
        public enum LogType
        {
            Database,
            Filesystem,
            Console
        }

        public string LogFilePath { set; get; }
        public LogType Type { get; set; }
        public DB Database { get; set; }

        // Signleton
        private static Logger instance = null;
        public static Logger Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Logger(LogType.Console);
                }
                return instance;
            }
        }

        public Logger(LogType type)
        {
            Type = type;
        }

        public Logger(LogType type, DB db)
        {
            Database = db;
            Type = type;
        }

        public Logger(LogType type, string logFile)
        {
            LogFilePath = logFile;
            Type = type;
        }

        public void Write(Object context, string text)
        {
            switch (Type)
            {
                case LogType.Database:
                    if (Database == null)
                    {
                        Log.C(context, text);
                        Console.WriteLine("Error: No db defined!");
                        return;
                    }
                    Log.D(context, text, Database);
                    break;
                case LogType.Filesystem:
                    if(LogFilePath == null)
                    {
                        Log.F(context, text);
                        return;
                    }
                    Log.F(context, text, LogFilePath);
                    break;
                case LogType.Console:
                default:
                    Log.C(context, text);
                    break;
            }
        }

        public void WriteLine(Object context, string text)
        {
            Write(context, text + "\n");
        }
    }

    /// <summary>
    /// Static logger
    /// </summary>
    public static class Log
    {
        private static string DEFAULT_LOG_PATH = @"log.txt";
        // File
        public static void F(Object context, string text)
        {
            F(context, text, DEFAULT_LOG_PATH);
        }
        public static void F(Object context, string text, string file)
        {
            if (createFileIfNotExcist() && File.Exists(file))
            {
                using (var tw = new StreamWriter(file, true))
                {
                    tw.WriteLine(getWriteText(context, text));
                }
            }
        }
        // Database
        public static void D(Object context, string text, DB database)
        {
            try
            {
                if (!database.CheckForTable("Log"))
                {
                    database.CreateTable("Log", "Id INT IDENTITY(1,1), Context VARCHAR(50), Message VARCHAR(100), Time VARCHAR(50)");
                }
                database.Insert("Log", "Context, Message, Time", "'" + context.GetType().ToString() + "'" +
                    ", '" + text + "', '" + DateTime.Now + "'");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: Can not init Log database!\n" + e.Message);
            }
        }
        // Console
        public static void C(Object context, string text)
        {
            Debug.Write(getWriteText(context, text)+"\n");
        }
        // Helper
        private static bool createFileIfNotExcist()
        {
            try
            {
                if (!File.Exists(DEFAULT_LOG_PATH))
                {
                    File.Create(DEFAULT_LOG_PATH);
                    TextWriter tw = new StreamWriter(DEFAULT_LOG_PATH);
                    tw.WriteLine("The very first line!");
                    tw.Close();
                }
                return true;
            }
            catch (Exception e)
            {
                Debug.Write("Error: Can not find and create log file!\n" + e.Message);
                return false;
            }
        }
        private static string getWriteText(Object context, string text)
        {
            return DateTime.Now.ToString() + " Log: " + context.GetType().ToString() + " => " + text;
        }
    }
}