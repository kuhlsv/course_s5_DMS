﻿using DMS_Client.DataAccess;
using DMS_Client.Database;
using DMS_Client.Logs;
using DMS_Client.Models;
using DMS_Client.ViewModels;
using System;
using System.Diagnostics;
using System.Windows;

namespace DMS_Client.Views
{
    /// <summary>
    /// Interaktionslogik für DMS.xaml
    /// </summary>
    public partial class DMS : Window
    {
        const string SEARCH_TEXT = "Search...";
        private UserViewModel currentUserVm;

        public DMS(UserViewModel currentUser)
        {
            this.currentUserVm = currentUser;
            InitializeComponent();
            this.Show();

            var fvm = FindResource("fcvm") as FileplanCollectionViewModel;
            DMSdb db = DMSdb.Instance;
            foreach(Fileplan fp in db.SelectAllFileplan())
            {
                fvm.Add(fp);
            }

            showUser();
            
            showAdminFunctions();
        }

        private void Tb_Search_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tb_search.Text))
            {
                tb_search.Text = SEARCH_TEXT;
            }
        }

        private void Tb_Search_GotFocus(object sender, RoutedEventArgs e)
        {
            if (tb_search.Text == SEARCH_TEXT)
            {
                tb_search.Text = "";
            }
        }

        private void showUser()
        {
            lbl_UserName.Content = this.currentUserVm.FirstName;
        }

        private void showAdminFunctions()
        {
            bool isAdmin = false;
            foreach(Role role in this.currentUserVm.Roles)
            {
                if(role.Name == "Admin")
                {
                    isAdmin = true;
                    break;
                }
            }

            if (isAdmin)
            {
                this.btnUserManagament.Visibility = Visibility.Visible;
            }else
            {
                this.btnUserManagament.Visibility = Visibility.Hidden;
            }
                
        }

        private void btnUserManagament_Click(object sender, RoutedEventArgs e)
        {
            new UserListing();
        }

        private void AddFiles(string[] files)
        {
            // Upload
            foreach (string file in files)
            {
                Log.C(this, file);
                DAL dal = DAL.Instance;
                // Store File
                string cryptedFileName = dal.InsertFile(file, 1.ToString());
                // Document
                DocumentViewModel newDocument = new DocumentViewModel();
                newDocument.Name = file.Substring(file.LastIndexOf(@"\") + 1);
                newDocument.CryptedName = cryptedFileName;
                newDocument.IsArchieved = false;
                newDocument.Category = "";
                newDocument.Owner = currentUserVm.Get();
                newDocument.Size = new System.IO.FileInfo(file).Length;
                newDocument.VersionNumber = 1;
                newDocument.Path = "";
                // Insert to Database
                if (!newDocument.SaveDocument())
                {
                    // Just store if insert DB is ok
                    dal.DeleteFile(cryptedFileName, false);
                }
                else
                {
                    // Fileplan
                    var fvm = FindResource("fcvm") as FileplanCollectionViewModel;
                    fvm.AddDocumentToSelected(newDocument);
                }
            }
        }

        private void File_Drop(object sender, DragEventArgs e)
        {
            var fvm = FindResource("fcvm") as FileplanCollectionViewModel;
            if (fvm.SelectedIndex != null)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    // Handle more than one file.
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    AddFiles(files);
                }
            }
        }

        private void btn_addfileplan_Click(object sender, RoutedEventArgs e)
        {
            string name = CreateFileplan.GetFilename();
            if(name.Length >= 3)
            {
                var fvm = FindResource("fcvm") as FileplanCollectionViewModel;
                fvm.AddFileplan(name);
            }
        }

        private void btn_export_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string path = tb_export.Text;
                string buffer = path.Substring(path.Length - 1, 1);
                if (buffer != @"\" && buffer != @"/")
                {
                    path = path + @"\";
                }
                if (path != "Outout Path:" && path != "")
                {
                    var fvm = FindResource("fcvm") as FileplanCollectionViewModel;
                    Log.C(this, "Export: " + path + fvm.Selected.Name);
                    DAL.Instance.GetFile(DAL.NetworkDrivePath +  @"\storage" + @"\" + fvm.Selected.CryptedName, 
                        path + fvm.Selected.Name);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var fvm = FindResource("fcvm") as FileplanCollectionViewModel;
                Log.C(this, "Delete: " + fvm.Selected.CryptedName);
                DAL.Instance.DeleteFile(fvm.Selected.CryptedName, fvm.Selected.IsArchieved);
                DMSdb.Instance.DeleteDocument(fvm.Selected);
                fvm.SelectedIndex.Documents.Remove(fvm.Selected);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }

        private void btn_open_Click(object sender, RoutedEventArgs e)
        {
                var fvm = FindResource("fcvm") as FileplanCollectionViewModel;
                string tmpFile = DAL.Instance.CopyToTemp(DAL.NetworkDrivePath + @"\storage" + @"\" + fvm.Selected.CryptedName, fvm.Selected.Name);
                Log.C(this, "Open: " + fvm.Selected.Name);
                Process.Start(tmpFile);
        }
    }
}
