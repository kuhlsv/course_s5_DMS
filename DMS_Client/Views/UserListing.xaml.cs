﻿using DMS_Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DMS_Client.Views
{
    /// <summary>
    /// Interaktionslogik für UserListing.xaml
    /// </summary>
    public partial class UserListing : Window
    {

        public UserListing()
        {
            InitializeComponent();
        
            this.Show();
        }

        private void newUser_Click(object sender, RoutedEventArgs e)
        {
           new UserProfile((UserCollectionViewModel)this.lvUsers.ItemsSource);
        }

        private void lvUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UserViewModel uvm = ((UserViewModel)e.AddedItems[0]);
            new UserProfile((UserCollectionViewModel)this.lvUsers.ItemsSource, uvm);
        }

   
    }
}
