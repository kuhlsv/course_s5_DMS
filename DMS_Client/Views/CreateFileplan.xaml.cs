﻿using DMS_Client.ViewModels;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DMS_Client.Views
{
    /// <summary>
    /// Interaktionslogik für CreateFileplan.xaml
    /// </summary>
    public partial class CreateFileplan : Window
    {
        public CreateFileplan()
        {
            InitializeComponent();
        }

        //CreateFileplan.GetFilename();
        public static string GetFilename()
        {
            CreateFileplan window = new CreateFileplan();
            window.ShowDialog();
            return window.Namecheck();
        }

        public string Namecheck()
        {
            return tb_name.Text;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
