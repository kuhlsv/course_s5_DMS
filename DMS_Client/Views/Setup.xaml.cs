﻿using DMS_Client.DataAccess;
using DMS_Client.Logs;
using DMS_Client.Models;
using DMS_Client.Views.Pages;
using System;
using System.IO;
using System.Windows;

namespace DMS_Client.Views
{
    /// <summary>
    /// Interaktionslogik für Setup.xaml
    /// </summary>
    public partial class Setup : Window
    {
        public int PageViewd { get; set; }
        public Configurations Config { get; set; }

        public Setup()
        {
            // Init
            InitializeComponent();
            Config = new Configurations();
            if(Config.LocalConfiguration == null)
            {
                ShowPage(1);
            }
            else if(Config.GobalConfiguration == null)
            {
                ShowPage(2);
            }
            else
            {
                Close();
            }
        }

        public Setup(int pageCount)
        {
            // Init
            InitializeComponent();
            Config = new Configurations();
            // Show specific page
            ShowPage(pageCount);
        }

        /// <summary>
        /// Show the page the view
        /// </summary>
        /// <param name="pageCount">Page to view (1 Path, 2 DB)</param>
        public void ShowPage(int pageCount)
        { 
            // Show page
            if (pageCount <= 1)
            {
                ContentFrame.Content = new SetupPath();
                PageViewd = 1;
            }
            else
            {
                ContentFrame.Content = new SetupDatabase();
                PageViewd = 2;
            }
        }

        private void GoOn_Click(object sender, RoutedEventArgs e)
        {
            if (PageViewd == 2)
            {
                if (Config.GobalConfiguration == null)
                {
                    Config.GobalConfiguration = new GlobalConfig();
                }
                // Read from window
                SetupDatabase setup = (SetupDatabase)ContentFrame.Content;
                Config.GobalConfiguration.DatabaseConfig.Ip = setup.tb_db_address.Text;
                int.TryParse(setup.tb_db_port.Text, out int port);
                Config.GobalConfiguration.DatabaseConfig.Port = port;
                Config.GobalConfiguration.DatabaseConfig.Name = setup.tb_db_name.Text;
                Config.GobalConfiguration.DatabaseConfig.User = setup.tb_db_user.Text;
                Config.GobalConfiguration.DatabaseConfig.Password = setup.tb_db_password.Text;
                // Return to Login
                Close();
            }
            else if (PageViewd == 1)
            {
                // Read from window
                SetupPath setup = (SetupPath)ContentFrame.Content;
                string path = setup.PathPicker.Text;
                Log.C(this, path);
                // Set
                if(Config.LocalConfiguration == null)
                {
                    Config.LocalConfiguration = new LocalConfig();
                }
                Config.LocalConfiguration.Path = path;
                Log.C(this,Config.LocalConfiguration.Path + ConfigReader.DEFAULT_KEY_NAME);
                Log.C(this,Config.LocalConfiguration.Path + ConfigReader.DEFAULT_DMS_NAME);
                if (!File.Exists(Config.LocalConfiguration.Path + ConfigReader.DEFAULT_KEY_NAME) ||
                    !File.Exists(Config.LocalConfiguration.Path + ConfigReader.DEFAULT_DMS_NAME))
                {
                    // Next page
                    ShowPage(2);
                }
                else
                {
                    // Return to Login
                    Close();
                }
            }
            else
            {
                // Return to Login
                Close();
            }
        }
    }
}
