﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DMS_Client.Views.UserControls
{
    /// <summary>
    /// Interaktionslogik für FolderEntryControl.xaml
    /// </summary>
    public partial class FileEntryControl : UserControl
    {
        public static DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(FileEntryControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty FilenameProperty = DependencyProperty.Register("Filename", typeof(string), typeof(FileEntryControl), new PropertyMetadata(null));

        public string Title { get { return GetValue(TitleProperty) as string; } set { SetValue(TitleProperty, value); } }

        public string Filename { get { return GetValue(FilenameProperty) as string; } set { SetValue(FilenameProperty, value); } }

        public FileEntryControl() {
            InitializeComponent();
        }

        private void BrowseFile(object sender, RoutedEventArgs e)
        {
            using (System.Windows.Forms.OpenFileDialog flg = new System.Windows.Forms.OpenFileDialog())
            {
                flg.Title = Title;
                flg.Filter = "key files (*.key)|*.key";
                System.Windows.Forms.DialogResult result = flg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    Filename = flg.FileName;
                    Console.WriteLine(flg.FileName);
                    BindingExpression be = GetBindingExpression(FilenameProperty);
                    if (be != null)
                        be.UpdateSource();
                }
            }
        }
    }
}
