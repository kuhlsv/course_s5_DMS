﻿using DMS_Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DMS_Client.Views
{
    /// <summary>
    /// Interaktionslogik für DMS.xaml
    /// </summary>
    public partial class UserProfile : Window
    {
        private UserCollectionViewModel ucvm;
        private UserViewModel uvm;
        private RoleCollectionViewModel rcwm;

         public UserProfile(UserCollectionViewModel ucvm)
        {
            InitializeComponent();
            this.ucvm = ucvm;
        
           
            
            this.Show();
        }
        public UserProfile(UserCollectionViewModel ucvm, UserViewModel uvm)
        {
            this.ucvm = ucvm;
            this.uvm = uvm;
            InitializeComponent();
            this.email.Text = uvm.Email;
            this.email.IsEnabled = false;
            this.firstname.Text = uvm.FirstName;
            this.lastname.Text = uvm.LastName;
            this.keepPassword.IsEnabled = true;
            this.keepPassword.Visibility = Visibility.Visible;
            this.keepPassword.IsChecked = true;
            // TODO in Listbox die aktuel gespeicherten Rollen markieren 
            
            //RoleCollectionViewModel rcvm = new RoleCollectionViewModel();
            //foreach(var role in uvm.Roles)
            //{
            //    RoleViewModel rvm = new RoleViewModel();

            //    rvm.Id = role.Id;
            //    rvm.Name = role.Name;
            //    rcvm.Add(rvm);
                
            //}
            
            this.Show();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            UserViewModel uvm = new UserViewModel();
           
            uvm.Email = this.email.Text;
            uvm.FirstName = this.firstname.Text;
            uvm.LastName = this.lastname.Text;
            uvm.Password = this.password.Text;
            uvm.InsertRoles(lbRoles.SelectedItems);

            //RoleViewModel rvm = new RoleViewModel();
            
            //foreach (var item in lbRoles.SelectedItems)
            //{
            //   Object b= item;
            //}

            uvm.SaveUser(keepPassword.IsChecked ?? false);
            ucvm.Add(uvm);

           

            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

      
    }
}
