﻿using DMS_Client.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DMS_Client.Views
{
    /// <summary>
    /// Interaktionslogik für Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            this.Show();
        }

        private void Btn_login_Click(object sender, RoutedEventArgs e)
        {
            login();
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();           
            this.Close();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                login();
            }
        }

        private void login()
        {
            UserViewModel uvm = new UserViewModel();
            UserViewModel current = uvm.CheckLogin(tb_email.Text, pb_password.Password);
            if (current != null)
            {
                this.Hide();
                new DMS(current);
                this.Close();
            }
            else
            {
                lbl_errMsg.Content = "Benutzerdaten falsch!";
                lbl_errMsg.Visibility = Visibility.Visible;
            }
        }
    }
}
