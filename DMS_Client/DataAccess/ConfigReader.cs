﻿using System;
using System.IO;
using System.Text;
using DMS_Client.Models;

namespace DMS_Client.DataAccess
{
    public static class ConfigReader
    {
        public const string DEFAULT_KEY_NAME = "\\crypt.key";
        public const string DEFAULT_DMS_NAME = "\\metadata.dms";
        public const string DEFAULT_CONFIG_NAME = "config.json";

        public static Configurations Parse(string localConfigPath, string cryptionKey)
        {
            LocalConfig lc = ParseLocalConfig(localConfigPath);
            return new Configurations(ParseGlobalConfig(lc.Path), lc);
        }

        public static LocalConfig ParseLocalConfig(string localConfigPath)
        {
            LocalConfig localConfig = null;
            // Get local config json
            string localConfigJson = null;
            try
            {
                using (StreamReader sr = new StreamReader(localConfigPath))
                {
                    localConfigJson = sr.ReadToEnd();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            // If could get local config json
            if (localConfigJson != null)
            {
                localConfig = LocalConfig.FromJson(localConfigJson);

            }
            return localConfig;
        }

        public static GlobalConfig ParseGlobalConfig(string globalConfigDirectoryPath)
        {
            GlobalConfig globalConfig = null;
            string globalConfigJson = Crypto.EncFileToPlainStringWithKeyFile(globalConfigDirectoryPath + DEFAULT_DMS_NAME, globalConfigDirectoryPath + DEFAULT_KEY_NAME);
            globalConfig = GlobalConfig.FromJson(globalConfigJson);
            return globalConfig;
        }

        /// <summary>
        /// Creates localConfig as config.json file in directory where executed.
        /// </summary>
        /// <param name="localConfig"></param>
        public static void CreateLocalConfig(LocalConfig localConfig)
        {
            CreateLocalConfig(DEFAULT_CONFIG_NAME, localConfig);
        }

        /// <summary>
        /// Creates localConfig.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="localConfig"></param>
        public static void CreateLocalConfig(string filePath, LocalConfig localConfig)
        {
            string localConfigsJson = Serialize.ToJsonLocal(localConfig);
            using (FileStream fs = File.Create(filePath))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(localConfigsJson);
                fs.Write(info, 0, info.Length);
            }
        }

        /// <summary>
        /// Creates globalConfig in globalPath directory (supposed to be network drive path). Is using encryption form Crypto class to encrypt global config.
        /// </summary>
        /// <param name="globalPath"></param>
        /// <param name="globalConfig"></param>
        /// <param name="key"></param>
        public static void CreateGlobalConfig(string globalPath, GlobalConfig globalConfig, string key)
        {
            // Create global key file
            Crypto.PlainStringToEncFile(key, globalPath + DEFAULT_KEY_NAME);

            // Create globalConfig file
            string globalConfigJson = Serialize.ToJsonGlobal(globalConfig);
            Crypto.PlainStringToEncFileWithKeyFile(globalConfigJson, globalPath + DEFAULT_DMS_NAME, globalPath + DEFAULT_KEY_NAME);
        }
    }
}
