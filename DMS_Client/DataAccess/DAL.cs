﻿using DMS_Client.DataAccess;
using DMS_Client.Logs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client
{
    public class DAL
    {
        private static DAL instance = null;
        private static readonly object padlock = new object();
        public static string NetworkDrivePath { get; set; }
        public const string STORAGE_DIRECTORY = "/storage";
        public const string ARCHIV_DIRECTORY = "/archive";

        private DAL()
        {
        }

        public static DAL Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DAL();
                    }
                    return instance;
                }
            }
        }

        public void SetNetworkDrivePath(string networkDrivePath)
        {
            NetworkDrivePath = networkDrivePath;
            try
            {
                Directory.CreateDirectory(networkDrivePath + STORAGE_DIRECTORY);
                Directory.CreateDirectory(networkDrivePath + ARCHIV_DIRECTORY);
            }
            catch (Exception e)
            {
                Log.C(this, "Error: " + e.Message);
            }
        }

        /*
         * DMS
         * */

        public string InsertFile(string inputFile, string fileVersion)
        {
            return Crypto.FileEncryptWithKeyFile(inputFile, fileVersion, NetworkDrivePath + STORAGE_DIRECTORY, NetworkDrivePath);
        }

        public void GetFile(string inputFile, string outputFile)
        {
            Crypto.FileDecryptWithKeyFile(inputFile, outputFile, NetworkDrivePath);
        }

        public string CopyToTemp(string inputFile, string outputFile)
        {
            // Get TEMP path
            string tmp = Path.GetTempPath() + @"dms\";
            bool exists = Directory.Exists(tmp);
            if (!exists)
            {
                Directory.CreateDirectory(tmp);
            }
            // Create Random
            Random ran = new Random();
            // Get File type
            string[] split = outputFile.Split('.');
            string newFileName = "";
            if (split.Length >= 2)
            {
                // Copy 
                newFileName = ran.Next(1000, 10000000) + "." + split[split.Length -1];
                Crypto.FileDecryptWithKeyFile(inputFile, tmp + newFileName, NetworkDrivePath);
            }
            return tmp + newFileName; //filepath
        }

        /// <summary>
        /// Deletes a File and returns an int greater than 0 if success. Else -1 if file not found, -2 file in use, -3 if no required permission or is executed or directory or read only file.
        /// </summary>
        /// <returns></returns>
        public int DeleteFile(string filename, bool isArchived)
        {
            string filePath = NetworkDrivePath + (!isArchived ? STORAGE_DIRECTORY : ARCHIV_DIRECTORY) + "/" + filename;
            int returnCode = 1;
            try
            {
                File.Delete(filePath);
            }
            catch(DirectoryNotFoundException dnfe)
            {
                Console.WriteLine(dnfe.Message);
                returnCode = -1;
            }
            catch(IOException ioe)
            {
                Console.WriteLine(ioe.Message);
                returnCode = -2;
            }
            catch(UnauthorizedAccessException uae)
            {
                Console.WriteLine(uae.Message);
                returnCode = -3;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                returnCode = -4;
            }

            return returnCode;
        }
    }
}
