﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.DataAccess
{
    class Crypto
    {
        private static readonly string privateProgrammKey = "W=RA+yQbVpE#h$_ApJ?M3fjT$Vj5B!6hy$7VgMThQb*wxwDeXhj+DtF4u?W7&HWm?AqY#K?j%H#9!Lub23zP%HPh37e_44XjqQsKhvfvA=Q75E-$*39FNfe^#LY4G82grf5TL9%&3uTa$CA$@!W$jUc^rC@Nw@tZ=xw#VTasV&4fmQX9$^?QYtf5^sWVUa6hPZZdkuhSbXG%gH*Da49FtgTB=!DCx@LLw4KUn7!6QyDAtsfczcEd+xcS3^4x2SzA";
        public static string KeyFileName = "crypt.key";

        /// <summary>
        /// Generates a random key by the length of generatedKeySize from the characters "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890+*-=?#_$%^@".
        /// </summary>
        /// <returns></returns>
        public static string GenerateKey(int generatedKeySize)
        {
            char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890+*-=?#_$%^@".ToCharArray();
            byte[] data = new byte[generatedKeySize];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(generatedKeySize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        /// <summary>
        /// Creates a random salt that will be used to encrypt your file. This method is required on FileEncrypt.
        /// </summary>
        /// <returns></returns>
        private static byte[] GenerateRandomSalt()
        {
            byte[] data = new byte[32];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                for (int i = 0; i < 10; i++)
                {
                    // Fille the buffer with the generated data
                    rng.GetBytes(data);
                }
            }

            return data;
        }

        /// <summary>
        /// Encrypts a file from its path using only the programm hardcoded key.
        /// </summary>
        /// <param name="fileValue"></param>
        /// <param name="fileOutputPath"></param>
        public static void PlainStringToEncFile(string fileValue, string fileOutputPath)
        {
            PlainStringToEncFile(fileValue, fileOutputPath, privateProgrammKey);
        }

        /// <summary>
        /// Encrypts a file from its path using only the programm hardcoded key.
        /// </summary>
        /// <param name="fileValue"></param>
        /// <param name="fileOutputPath"></param>
        public static bool PlainStringToEncFileWithKeyFile(string fileValue, string fileOutputPath, string keyPath)
        {
            if (File.Exists(keyPath))
            {
                // Get key
                string key = EncFileToPlainString(keyPath);
                PlainStringToEncFile(fileValue, fileOutputPath, key);
                return true;
            }
            else
            {
                // TODO Log that key file is not existing.
                return false;
            }
        }

        public static void PlainStringToEncFile(string fileValue, string fileOutputPath, string key)
        {
            // Generate random salt
            byte[] salt = GenerateRandomSalt();

            // Create output file name
            FileStream fsCrypt = new FileStream(fileOutputPath, FileMode.Create);

            // Convert key string to byte arrray
            byte[] keyBytes = System.Text.Encoding.UTF8.GetBytes(key);

            // Set Rijndael symmetric encryption algorithm.
            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            AES.Padding = PaddingMode.PKCS7;

            // Hash the key with the salt. High iteration counts.
            var key2 = new Rfc2898DeriveBytes(keyBytes, salt, 50000);
            AES.Key = key2.GetBytes(AES.KeySize / 8);
            AES.IV = key2.GetBytes(AES.BlockSize / 8);
            AES.Mode = CipherMode.CFB;

            // Write salt to the begining of the output file.
            fsCrypt.Write(salt, 0, salt.Length);

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateEncryptor(), CryptoStreamMode.Write);

            byte[] bytes = Encoding.UTF8.GetBytes(fileValue);

            try
            {
                cs.Write(bytes, 0, bytes.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                cs.Close();
                fsCrypt.Close();
            }
        }


        public static string EncFileToPlainString(string filePath)
        {
            return EncFileToPlainString(filePath, privateProgrammKey);
        }

        public static string EncFileToPlainStringWithKeyFile(string filePath, string keyPath)
        {
            string key = EncFileToPlainString(keyPath);
            return EncFileToPlainString(filePath, key);
        }

        public static string EncFileToPlainString(string filePath, string inputKey)
        {
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(inputKey);
            byte[] salt = new byte[32];

            FileStream fsCrypt = new FileStream(filePath, FileMode.Open);
            fsCrypt.Read(salt, 0, salt.Length);

            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CFB;

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);


            List<byte> keyByteList = new List<byte>();

            int read;
            byte[] buffer = new byte[1048576];

            try
            {
                while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                {

                    for (int i = 0; i < read; i++)
                    {
                        keyByteList.Add(buffer[i]);
                    }
                }
            }
            catch (CryptographicException ex_CryptographicException)
            {
                Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            try
            {
                cs.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
            }
            finally
            {
                fsCrypt.Close();
            }
            return Encoding.UTF8.GetString(keyByteList.ToArray());
        }

        /// <summary>
        /// Encrypts a file from its path using key file from keyPath.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="fileVersion"></param>
        /// <param name="outputPath"></param>
        /// <param name="keyPath"></param>
        public static string FileEncryptWithKeyFile(string inputFile, string fileVersion, string outputPath, string keyPath)
        {
            string fileName = GetHashString(SeparateFilePathAndFileName(inputFile)[1] + fileVersion);
            FileEncrypt(inputFile, outputPath + "\\" + fileName, EncFileToPlainString(keyPath + "\\" + KeyFileName));
            return fileName;
        }

        /// <summary>
        /// Encrypts a file from its path and a plain password.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        /// <param name="inputKey"></param>
        private static void FileEncrypt(string inputFile, string outputFile, string inputKey)
        {
            // Generate random salt
            byte[] salt = GenerateRandomSalt();

            // Create output file name
            FileStream fsCrypt = new FileStream(outputFile, FileMode.Create);

            //convert password string to byte arrray
            byte[] keyBytes = System.Text.Encoding.UTF8.GetBytes(inputKey);

            //Set Rijndael symmetric encryption algorithm
            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            AES.Padding = PaddingMode.PKCS7;

            // Hash the key with the salt. High iteration counts.
            var key = new Rfc2898DeriveBytes(keyBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Mode = CipherMode.CFB;

            // Write salt to the begining of the output file.
            fsCrypt.Write(salt, 0, salt.Length);

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateEncryptor(), CryptoStreamMode.Write);

            FileStream fsIn = new FileStream(inputFile, FileMode.Open);

            // Create a buffer (1mb) so only this amount will allocate in the memory and not the whole file
            byte[] buffer = new byte[1048576];
            int read;

            try
            {
                while ((read = fsIn.Read(buffer, 0, buffer.Length)) > 0)
                {
                    cs.Write(buffer, 0, read);
                }

                // Close up
                fsIn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                cs.Close();
                fsCrypt.Close();
            }
        }

        /// <summary>
        /// Decrypts an encrypted file with the FileEncrypt method through its path using key file from keyPath.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        /// <param name="keyPath"></param>
        public static void FileDecryptWithKeyFile(string inputFile, string outputFile, string keyPath)
        {
            FileDecrypt(inputFile, outputFile, EncFileToPlainString(keyPath + "\\" + KeyFileName));
        }

        /// <summary>
        /// Decrypts an encrypted file with the FileEncrypt method through its path and the plain password.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        /// <param name="inputKey"></param>
        private static void FileDecrypt(string inputFile, string outputFile, string inputKey)
        {
            byte[] keyBytes = System.Text.Encoding.UTF8.GetBytes(inputKey);
            byte[] salt = new byte[32];

            FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
            fsCrypt.Read(salt, 0, salt.Length);

            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(keyBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CFB;

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);

            FileStream fsOut = new FileStream(outputFile, FileMode.Create);

            int read;
            byte[] buffer = new byte[1048576];

            try
            {
                while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fsOut.Write(buffer, 0, read);
                }
            }
            catch (CryptographicException ex_CryptographicException)
            {
                Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            try
            {
                cs.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
            }
            finally
            {
                fsOut.Close();
                fsCrypt.Close();
            }
        }

        // Filename Part

        private static string[] SeparateFilePathAndFileName(string filePathName)
        {
            string fileName = filePathName.Split(new char[] { '/', '\\' }).Reverse().ToArray()[0];
            string filePath = filePathName.Replace(fileName, "");
            return new string[] { filePath, fileName };
        }

        // String Hash

        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
