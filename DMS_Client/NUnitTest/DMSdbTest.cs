﻿using DMS_Client.Database;
using DMS_Client.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.NUnitTest
{
    [TestFixture]
    class DMSdbTest
    {
        DMSdb dmsdb = DMSdb.Instance;
        public const string DEV_DB_CONN_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=DMS_Test;Integrated Security=True;";

        // User dummy data
        User preDBTestUser;
        User postDBTestUser;

        // Document dummy data
        Document preDBTestDocument;
        Document postDBTestDocument;

        // Fileplan dummy data
        Fileplan preDBTestFileplan;
        Fileplan postDBTestFileplan;
        Fileplan parentFileplan;

        // Role dummy data
        Role preDBTestRole;
        Role postDBTestRole;

        [SetUp]
        public void Init()
        {

            dmsdb.SetConnection(DEV_DB_CONN_STRING);
            //dmsdb.QueryBool("DROP TABLE ?");
            dmsdb.InstallScript();
            // User
            preDBTestUser = new User("Max", "Mustermann", "max.mustermann@nunit.test", "asdf1234", null);
            // Document
            preDBTestDocument = new Document("TestDocument");
            preDBTestDocument.DocId = 1234;
            preDBTestDocument.CryptedName = "slkdjfksjdkljslkjc";
            preDBTestDocument.Path = "C://myDocs/file.doc";
            preDBTestDocument.Type = ".doc";
            preDBTestDocument.Category = "important";
            preDBTestDocument.Signatur = "asdf";
            preDBTestDocument.VersionNumber = 1;
            preDBTestDocument.IsArchieved = false;
            preDBTestDocument.Size = 1234567.89;
            preDBTestDocument.Owner = preDBTestUser;
            // Fileplan
            parentFileplan = new Fileplan("ParentFileplan", preDBTestDocument);

            preDBTestFileplan = new Fileplan("TestPlan", preDBTestDocument);
            preDBTestFileplan.Filenumber = 1234;
            preDBTestFileplan.Parent = parentFileplan;
            preDBTestFileplan.Read = new List<Role>();
            preDBTestFileplan.ReadWrite = new List<Role>();
            // Role
            preDBTestRole = new Role();
            preDBTestRole.Id = 1234;
            preDBTestRole.Name = "Master";
        }

        /*
         * User Testing
         */

        [TestCase]
        public void InsertUser_SelectUser_DeleteUser()
        {
            // Insert User
            Assert.True(dmsdb.InsertUser(preDBTestUser));

            // Select User
            postDBTestUser = dmsdb.SelectUser(preDBTestUser.Email);
            Assert.AreEqual(postDBTestUser.FirstName, preDBTestUser.FirstName);
            Assert.AreEqual(postDBTestUser.LastName, preDBTestUser.LastName);
            Assert.AreEqual(postDBTestUser.Email, preDBTestUser.Email);
            Assert.AreEqual(postDBTestUser.Password, preDBTestUser.Password);

            // Delete User
            Assert.True(dmsdb.DeleteUser(preDBTestUser));
        }

        [TestCase]
        public void DeleteNotExistingUser()
        {
            Assert.False(dmsdb.DeleteUser(preDBTestUser));
        }

        [TestCase]
        public void DeleteUser()
        {
            dmsdb.InsertUser(preDBTestUser);
            Assert.True(dmsdb.DeleteUser(preDBTestUser));
        }

        [TestCase]
        public void UpdateUser()
        {
            dmsdb.InsertUser(preDBTestUser);
            User updatedUser = new User("Erika", "Musterfrau", preDBTestUser.Email , "123456789", null);
            Assert.True(dmsdb.UpdateUser(updatedUser));

            // Select updated User
            postDBTestUser = dmsdb.SelectUser(preDBTestUser.Email);
            Assert.AreEqual(postDBTestUser.FirstName, updatedUser.FirstName);
            Assert.AreEqual(postDBTestUser.LastName, updatedUser.LastName);
            Assert.AreEqual(postDBTestUser.Email, updatedUser.Email);
            Assert.AreEqual(postDBTestUser.Password, updatedUser.Password);

            dmsdb.DeleteUser(postDBTestUser);
        }

        /*
         * Document Testing
         */
         
        [TestCase]
        public void InsertDocument_SelectDocument_DeleteDocument()
        {
            // Insert Document
            Assert.True(dmsdb.InsertDocument(preDBTestDocument));

            // Select Document
            postDBTestDocument = dmsdb.SelectDocument(preDBTestDocument.DocId, preDBTestDocument.VersionNumber);
            Assert.AreEqual(postDBTestDocument.DocId, preDBTestDocument.DocId);
            Assert.AreEqual(postDBTestDocument.Name, preDBTestDocument.Name);
            Assert.AreEqual(postDBTestDocument.CryptedName, preDBTestDocument.CryptedName);
            Assert.AreEqual(postDBTestDocument.Path, preDBTestDocument.Path);
            Assert.AreEqual(postDBTestDocument.Type, preDBTestDocument.Type);
            Assert.AreEqual(postDBTestDocument.Category, preDBTestDocument.Category);
            Assert.AreEqual(postDBTestDocument.Signatur, preDBTestDocument.Signatur);
            Assert.AreEqual(postDBTestDocument.VersionNumber, preDBTestDocument.VersionNumber);
            Assert.AreEqual(postDBTestDocument.IsArchieved, preDBTestDocument.IsArchieved);
            Assert.AreEqual(postDBTestDocument.Size, preDBTestDocument.Size);
            Assert.AreEqual(postDBTestDocument.Owner.Email, preDBTestDocument.Owner.Email);

            // Delete Document
            Assert.True(dmsdb.DeleteDocument(preDBTestDocument));
        }

        [TestCase]
        public void DeleteNotExistingDocument()
        {
            Assert.False(dmsdb.DeleteDocument(preDBTestDocument));
        }

        [TestCase]
        public void DeleteDocument()
        {
            dmsdb.InsertDocument(preDBTestDocument);
            Assert.True(dmsdb.DeleteDocument(preDBTestDocument));
        }

        [TestCase]
        public void UpdateDocument()
        {
            dmsdb.InsertDocument(preDBTestDocument);

            Document updatedDocument = new Document("UpdatedDocument");
            updatedDocument.DocId = 1234; // Dont change docId, is identifier
            updatedDocument.CryptedName = "oskkcldkdlk";
            updatedDocument.Path = "C://yourDocs/file.docx";
            updatedDocument.Type = ".docx";
            updatedDocument.Category = "unimportant";
            updatedDocument.Signatur = "vssdf";
            updatedDocument.VersionNumber = 1; // Dont change versionNumber, is identifier
            updatedDocument.IsArchieved = true;
            updatedDocument.Size = 98765.432;
            updatedDocument.Owner = preDBTestUser;

            Assert.True(dmsdb.UpdateDocument(updatedDocument));

            // Select updated User
            postDBTestDocument = dmsdb.SelectDocument(preDBTestDocument.DocId, preDBTestDocument.VersionNumber);
            Assert.AreEqual(postDBTestDocument.DocId, preDBTestDocument.DocId);
            Assert.AreEqual(postDBTestDocument.Name, preDBTestDocument.Name);
            Assert.AreEqual(postDBTestDocument.CryptedName, preDBTestDocument.CryptedName);
            Assert.AreEqual(postDBTestDocument.Path, preDBTestDocument.Path);
            Assert.AreEqual(postDBTestDocument.Type, preDBTestDocument.Type);
            Assert.AreEqual(postDBTestDocument.Category, preDBTestDocument.Category);
            Assert.AreEqual(postDBTestDocument.Signatur, preDBTestDocument.Signatur);
            Assert.AreEqual(postDBTestDocument.VersionNumber, preDBTestDocument.VersionNumber);
            Assert.AreEqual(postDBTestDocument.IsArchieved, preDBTestDocument.IsArchieved);
            Assert.AreEqual(postDBTestDocument.Size, preDBTestDocument.Size);
            Assert.AreEqual(postDBTestDocument.Owner.Email, preDBTestDocument.Owner.Email);

            dmsdb.DeleteDocument(postDBTestDocument);
        }

        /*
         * Fileplan Testing
         */
        
        [TestCase]
        public void InsertFileplan_SelectFileplan_DeleteFileplan()
        {
            // Insert Fileplan
            Assert.True(dmsdb.InsertFileplan(preDBTestFileplan));

            // Select Fileplan
            postDBTestFileplan = dmsdb.SelectFileplan(1234);
            Assert.AreEqual(postDBTestFileplan.Filenumber, preDBTestFileplan.Filenumber);
            Assert.AreEqual(postDBTestFileplan.Name, preDBTestFileplan.Name);
            Assert.AreEqual(postDBTestFileplan.Parent.Name, preDBTestFileplan.Parent.Name);
            Assert.AreEqual(postDBTestFileplan.Documents[0].DocId, preDBTestFileplan.Documents[0].DocId);
            Assert.AreEqual(postDBTestFileplan.Read, preDBTestFileplan.Read);
            Assert.AreEqual(postDBTestFileplan.ReadWrite, preDBTestFileplan.ReadWrite);

            // Delete Fileplan
            Assert.True(dmsdb.DeleteFileplan(preDBTestFileplan));
        }

        [TestCase]
        public void DeleteNotExistingFileplan()
        {
            Assert.False(dmsdb.DeleteFileplan(preDBTestFileplan));
        }

        [TestCase]
        public void DeleteFileplan()
        {
            dmsdb.InsertFileplan(preDBTestFileplan);
            Assert.True(dmsdb.DeleteFileplan(preDBTestFileplan));
        }

        [TestCase]
        public void UpdateFileplan()
        {
            dmsdb.InsertFileplan(preDBTestFileplan);

            Fileplan updatedFileplan = new Fileplan("UpdatedFileplan", preDBTestDocument);
            updatedFileplan.Filenumber = 1234;
            updatedFileplan.Parent = parentFileplan;
            updatedFileplan.Read = null;
            updatedFileplan.ReadWrite = null;
            

            Assert.True(dmsdb.UpdateFileplan(updatedFileplan));

            // Select updated User
            postDBTestFileplan = dmsdb.SelectFileplan(1234);
            Assert.AreEqual(postDBTestFileplan.Filenumber, postDBTestFileplan.Filenumber);
            Assert.AreEqual(postDBTestFileplan.Parent.Name, postDBTestFileplan.Parent.Name);
            Assert.AreEqual(postDBTestFileplan.Read, postDBTestFileplan.Read);
            Assert.AreEqual(postDBTestFileplan.ReadWrite, postDBTestFileplan.ReadWrite);
            Assert.AreEqual(postDBTestFileplan.Name, postDBTestFileplan.Name);
            Assert.AreEqual(postDBTestFileplan.Documents[0].DocId, postDBTestFileplan.Documents[0].DocId);

            dmsdb.DeleteFileplan(updatedFileplan);
        }

        /*
         * Role Testing
         */

        [TestCase]
        public void InsertRole_SelectRole_DeleteRole()
        {
            // Insert Role
            Assert.True(dmsdb.InsertRole(preDBTestRole));

            // Select Role
            postDBTestRole = dmsdb.SelectRole(1234);
            Assert.AreEqual(postDBTestRole.Id, preDBTestRole.Id);
            Assert.AreEqual(postDBTestRole.Name, preDBTestRole.Name);
           
            // Delete Fileplan
            Assert.True(dmsdb.DeleteRole(preDBTestRole));
        }

        [TestCase]
        public void DeleteNotExistingRole()
        {
            Assert.False(dmsdb.DeleteRole(preDBTestRole));
        }

        [TestCase]
        public void DeleteRole()
        {
            dmsdb.InsertRole(preDBTestRole);
            Assert.True(dmsdb.DeleteRole(preDBTestRole));
        }

        [TestCase]
        public void UpdateRole()
        {
            dmsdb.InsertRole(preDBTestRole);

            Role updatedRole = new Role();
            updatedRole.Id = 1234;
            updatedRole.Name = "Slave";


            Assert.True(dmsdb.UpdateRole(updatedRole));

            // Select updated User
            postDBTestFileplan = dmsdb.SelectFileplan(1234);
            Assert.AreEqual(postDBTestRole.Id, postDBTestRole.Id);
            Assert.AreEqual(postDBTestRole.Name, postDBTestRole.Name);

            dmsdb.DeleteRole(updatedRole);
        }
    }
}
