﻿using DMS_Client.DataAccess;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.NUnitTest
{
    [TestFixture]
    class CryptopTest
    {
        [TestCase]
        public void GenerateKey()
        {
            for(int i = 0; i < 100; i++)
            {
                Assert.AreEqual(i, Crypto.GenerateKey(i).Length);
            }
        }
    }
}
