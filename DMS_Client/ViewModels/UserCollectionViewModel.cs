﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using DMS_Client.Models;
using DMS_Client.Database;
using System.Collections.Specialized;
using DMS_Client.Views;

namespace DMS_Client.ViewModels
{
    public class UserCollectionViewModel : ObservableCollection<UserViewModel>
    {
        private int selected;
        private UserCollection userCollection;
        public UserCollectionViewModel() {
            // Initialisation
            this.userCollection = new UserCollection();

            //DB 

            DMSdb db = DMSdb.Instance;
            this.userCollection = db.SelectAllUsers();

            userCollection.CollectionChanged += ModelCollectionChanged;


            this.selected = -1;

            this.FetchCollection();
        }

        private void ModelCollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            this.FetchCollection();
        }
        /// <summary> 
        /// Public attribute to set selected index from UI.
        /// </summary>
        /// 
        public int SelectedIndex
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
            }
        }

        private void FetchCollection()
        {
            foreach (User user in userCollection)
            {
                this.Add(new UserViewModel(user));
            }
        }
    }
}
