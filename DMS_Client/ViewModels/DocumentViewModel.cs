﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMS_Client.Database;
using DMS_Client.Models;

namespace DMS_Client.ViewModels
{
    public class DocumentViewModel
    {
        Document doc = new Document();

        public DocumentViewModel() { }
        public DocumentViewModel(Document doc)
        {
            this.doc = doc;
        }

        public int DocId
        {
            get
            {
                return doc.DocId;
            }
            set
            {
                doc.DocId = value;
            }
        }
        public string Name
        {
            get
            {
                return doc.Name;
            }
            set
            {
                doc.Name = value;
            }
        }
        public string CryptedName
        {
            get
            {
                return doc.CryptedName;
            }
            set
            {
                doc.CryptedName = value;
            }
        }
        public string Path
        {
            get
            {
                return doc.Path;
            }
            set
            {
                doc.Path = value;
            }
        }
        public string Type
        {
            get 
            {
                return doc.Type;
            }
            set
            {
                doc.Type = value;
            }
        }
        public string Category
        {
            get
            {
                return doc.Category;
            }
            set
            {
                doc.Category = value;
            }
        }
        public string Signatur
        {
            get
            {
                return doc.Signatur;
            }
            set
            {
                doc.Signatur = value;
            }
        }
        public int VersionNumber
        {
            get
            {
                return doc.VersionNumber;
            }
            set
            {
                doc.VersionNumber = value;
            }
        }
        public bool IsArchieved
        {
            get
            {
                return doc.IsArchieved;
            }
            set
            {
                doc.IsArchieved = value;
            }
        }
        public double Size
        {
            get
            {
                return doc.Size;
            }
            set
            {
                doc.Size = value;
            }
        }
        public User Owner
        {
            get
            {
                return doc.Owner;
            }
            set
            {
                doc.Owner = value;
            }
        }

        public Document Get()
        {
            return doc;
        }

        public bool SaveDocument()
        {
            DMSdb db = DMSdb.Instance;
            Document foundDoc = db.SelectDocument(this.DocId, this.VersionNumber);
            if (foundDoc.DocId > 0)
            {
               return db.UpdateDocument(this.doc);
            }else
            {
               return db.InsertDocument(this.doc);
            }
        }
    }
}
