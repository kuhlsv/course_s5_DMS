﻿using DMS_Client.DataAccess;
using DMS_Client.Views;
using System.IO;
using DMS_Client.Models;
using System;
using System.ComponentModel;
using DMS_Client.Database;

namespace DMS_Client.ViewModels
{
    class SetupViewModel
    {
        public const string DEV_DB_CONN_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=DMS;Integrated Security=True;";
        public const string DEFAULT_MODE = "develop";

        public SetupViewModel()
        { 
            Setup setup;
            // Get Configurations
            Configurations config = GetConfiguration();
            // Show path select
            if (config.LocalConfiguration == null || config.LocalConfiguration.Path == null)
            {
                config.LocalConfiguration = new LocalConfig
                {
                    Mode = DEFAULT_MODE
                };
                // Check if allready configured
                setup = new Setup(1)
                {
                    Config = config
                };
                setup.Closing += OnWindowClosing;
                setup.Show();
            }
            // Show database
            else if (config.GobalConfiguration == null || config.GobalConfiguration.DatabaseConfig == null)
            {
                // Check if allready configured
                setup = new Setup(2)
                {
                    Config = config
                };
                setup.Closing += OnWindowClosing;
                setup.Show();
            }
            else
            {
                Init(config);
            }
            // Set path to DAL
            DAL.Instance.SetNetworkDrivePath(config.LocalConfiguration.Path);
        }

        public void Init(Configurations config)
        {
            // Database
            if(config.LocalConfiguration.Mode == "develop")
            {
                DMSdb.Instance.SetConnection(DEV_DB_CONN_STRING);
            }
            else
            {
                DMSdb.Instance.SetConnection(config.GobalConfiguration.DatabaseConfig.Ip,
                    config.GobalConfiguration.DatabaseConfig.Port,
                    config.GobalConfiguration.DatabaseConfig.Name,
                    config.GobalConfiguration.DatabaseConfig.User,
                    config.GobalConfiguration.DatabaseConfig.Password);
            }
            DMSdb.Instance.InstallScript();
            // Login
            new Login();
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            Setup setup = (Setup)sender;
            if (setup.PageViewd == 1)
            {
                CreateLocal(setup.Config);
                if(setup.Config.GobalConfiguration == null)
                {
                    setup.Config.GobalConfiguration = GetGlobalFromConfig(setup.Config.LocalConfiguration.Path);
                }
            }
            else if (setup.PageViewd == 2)
            {
                CreateLocal(setup.Config);
                CreateGlobal(setup.Config);
            }
            /// INIT
            Init(setup.Config);
        }

        private Configurations GetConfiguration()
        {
            LocalConfig localConfig = null;
            GlobalConfig globalConfig = null;
            // Local config
            if (File.Exists(ConfigReader.DEFAULT_CONFIG_NAME))
            {
                localConfig = GetLocalFromConfig();
            }
            // Global config
            if (localConfig != null && File.Exists(localConfig.Path + ConfigReader.DEFAULT_KEY_NAME) && File.Exists(localConfig.Path + ConfigReader.DEFAULT_DMS_NAME))
            {
                globalConfig = GetGlobalFromConfig(localConfig.Path);
            }
            return new Configurations(globalConfig, localConfig);
        }

        /*
         * Local Config
         * */

        private LocalConfig CreateLocal(Configurations config)
        {
            LocalConfig localConfig = new LocalConfig();
            localConfig.Path = config.LocalConfiguration.Path;
            localConfig.Mode = config.LocalConfiguration.Mode;
            ConfigReader.CreateLocalConfig(localConfig);
            return new LocalConfig();
        }

        private LocalConfig GetLocalFromConfig()
        {
            return ConfigReader.ParseLocalConfig(ConfigReader.DEFAULT_CONFIG_NAME);
        }

        /*
         * Global Connfig
         * */

        /// <summary>
        /// Gets globalConfig from globalPath (supposed to be network drive path). If no globalConfig exists returns null
        /// </summary>
        /// <param name="path">Global config ath</param>
        private GlobalConfig GetGlobalFromConfig(string path)
        {
            GlobalConfig globalConfig = null;
            if (File.Exists(path + ConfigReader.DEFAULT_KEY_NAME) && File.Exists(path + ConfigReader.DEFAULT_DMS_NAME))
            {
                globalConfig = ConfigReader.ParseGlobalConfig(path);
            }
            return globalConfig;
        }

        private void CreateGlobal(Configurations config)
        {
            Random rnd = new Random();
            int id = rnd.Next(1000000);
            GlobalConfig globalConfig = config.GobalConfiguration;
            globalConfig.DatabaseConfig.DmsId = id.ToString();
            globalConfig.DatabaseConfig.ConnectionString = DEV_DB_CONN_STRING;
            // Generate key
            string key = Crypto.GenerateKey(60);
            ConfigReader.CreateGlobalConfig(config.LocalConfiguration.Path, globalConfig, key);
        }
    }
}
