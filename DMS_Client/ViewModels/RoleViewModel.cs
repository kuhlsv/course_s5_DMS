﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMS_Client.Models;

namespace DMS_Client.ViewModels
{
    public class RoleViewModel
    {
        private Role role = new Role();

        public RoleViewModel() { }

        public RoleViewModel(Role role)
        {
            this.role = role;   
        }

        public string Name
        {
            set { role.Name = value; }
            get { return role.Name; }
        }

        public int Id
        {
            set { role.Id = value; }
            get { return role.Id; }
        }
             
    }
}
