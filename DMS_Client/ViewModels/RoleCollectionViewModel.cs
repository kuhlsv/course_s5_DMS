﻿using DMS_Client.Database;
using DMS_Client.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.ViewModels
{
    class RoleCollectionViewModel : ObservableCollection<RoleViewModel>
    {
        private int selected;
        private RoleCollection roleCollection;
        public RoleCollectionViewModel()
        {
            // Initialisation
            this.roleCollection = new RoleCollection();

            //DB 

            DMSdb db = DMSdb.Instance;
            this.roleCollection = db.SelectAllRoles();

            this.selected = -1;

            this.FetchCollection();
        }
        private void FetchCollection()
        {
            foreach (Role role in roleCollection)
            {
                this.Add(new RoleViewModel(role));
            }
        }
    }
}
