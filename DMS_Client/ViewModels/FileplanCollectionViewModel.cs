﻿using DMS_Client.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DMS_Client.Database;

namespace DMS_Client.ViewModels
{
    /// <summary>
    /// The ViewModel for the DMS Content.
    /// It represents a Collection of Fileplan for the left sidebar
    /// </summary>
    class FileplanCollectionViewModel : ObservableCollection<Fileplan>, INotifyPropertyChanged
    {
        private int selected;
        private DocumentCollection documents;
        #pragma warning disable CS0114
        public event PropertyChangedEventHandler PropertyChanged;
        #pragma warning restore CS0114

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public FileplanCollectionViewModel()
        {
            // Initialisation
            this.selected = -1;
        }

        public bool AddFileplan(string name)
        {
            Fileplan fp = new Fileplan(name);
            Add(fp);
            // Add to database
            DMSdb db = DMSdb.Instance;
            return db.InsertFileplan(fp);
        }

        /// <summary> 
        /// Public attribute to set selected index from UI.
        /// </summary>
        public Fileplan SelectedIndex
        {
            get
            {
                if (selected == -1)
                {
                    return null;
                }
                return this[selected];
            }
            set
            {
                selected = IndexOf(value);
                documents = value.Documents;
                NotifyPropertyChanged("Documents");
                NotifyPropertyChanged("Selected");
            }
        }

        public Document Selected
        {
            get
            {
                if (selected >= 0)
                {
                    return this[selected].Documents.Selected;
                }
                return null;
            }
            set
            {
                if (selected >= 0)
                {
                    this[selected].Documents.Selected = value;
                }
            }
        }


        public int SelectedDocument
        {
            get
            {
                if (selected >= 0)
                {
                    return this[selected].Documents.SelectedIndex;
                }
                return 0;
            }
            set
            {
                if (selected >= 0)
                {
                    this[selected].Documents.SelectedIndex = value;
                    NotifyPropertyChanged("Selected");
                }
            }
        }

        public DocumentCollection Documents
        {
            get
            {
                return documents;
            }
            set
            {
                documents = value;
            }
        }

        public bool AddDocumentToSelected(DocumentViewModel doc)
        {
            DMSdb db = DMSdb.Instance;
            Fileplan foundFileplan = db.SelectFileplan(this[selected].Filenumber);
            this[selected].Documents.Add(doc.Get());
            if (foundFileplan.Filenumber != 0)
            {
                return db.UpdateFileplan(this[selected]);
            }
            else
            {
                return db.InsertFileplan(this[selected]);
            }
        }
    }
}
