﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMS_Client.Database;
using DMS_Client.Models;

namespace DMS_Client.ViewModels
{
    public class UserViewModel
    {
        private User user = new User();

        public UserViewModel () { }
        public UserViewModel(User user)
        {
            this.user = user;
        }

        public string FirstName
        {
            set { user.FirstName = value; }
            get { return user.FirstName; }
        }

        public string LastName
        {
            set { user.LastName = value; }
            get { return user.LastName; }
        }

        public string Email
        {
            set { user.Email = value; }
            get { return user.Email; }
        }

        public string Password
        {
            set {
                user.Password = value;
            }
            get { return user.Password; }
        }

        public List<Role> Roles
        {
            set { user.Roles = value; }
            get { return user.Roles; }
        }

        public User Get()
        {
            return user;
        }

        public void SaveUser(bool keepPassword)
        {
           DMSdb db = DMSdb.Instance;
           User foundUser = db.SelectUser(this.Email);
            if (foundUser.Email != null)
            {
                if(keepPassword)
                {
                    this.user.Password = foundUser.Password;
                }
                db.UpdateUser(this.user);
            }else
            {
                db.InsertUser(this.user);
                
            }
        }

        public UserViewModel CheckLogin(string email, string password)
        {
            UserViewModel current = null;

            DMSdb db = DMSdb.Instance;
            User foundUser = db.SelectUser(email);
            if(foundUser.Email != null)
            {
                if(foundUser.Password == password)
                {
                    // Login ok  
                    current = new UserViewModel(foundUser);                     
                }
            }
            
            return current;
        }

        internal void InsertRoles(IList selectedItems)
        {
            foreach(RoleViewModel item in selectedItems)
            {
                Role role = new Role();
                role.Id = item.Id;
                role.Name = item.Name;
                Roles.Add(role);
            }
            
        }
    }
}
