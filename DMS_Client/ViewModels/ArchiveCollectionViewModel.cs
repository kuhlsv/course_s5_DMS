﻿using DMS_Client.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.ViewModels
{
    /// <summary>
    /// The ViewModel for the DMS Content.
    /// It represents a Archive Collection of Documents (Archived) by selected Fileplan (Archive) for the List
    /// </summary>
    class ArchiveCollectionViewModel : ObservableCollection<Document>
    {
        private int selected;

        public ArchiveCollectionViewModel()
        {
            // Initialisation
            this.selected = -1;
        }

        /// <summary> 
        /// Public attribute to set selected index from UI.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
            }
        }
    }
}
