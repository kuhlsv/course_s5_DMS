﻿using System;
using System.Collections.ObjectModel;

namespace DMS_Client.Models
{
    public class DocumentCollection : ObservableCollection<Document>
    {
        private int selected;

        public DocumentCollection()
        {
            // Initialisation
            this.selected = 0;
        }

        /// <summary> 
        /// Public attribute to set selected index from UI.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
            }
        }

        public Document Selected
        {
            get
            {
                if(selected < 0 || this.Count == 0)
                {
                    return null;
                }
                return this[selected];
            }
            set
            {
                if (selected >= 0)
                {
                    this[selected] = value;
                }
            }
        }
    }
}
