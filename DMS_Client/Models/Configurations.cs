﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;

namespace DMS_Client.Models
{
    public class Configurations
    {
        public GlobalConfig GobalConfiguration;
        public LocalConfig LocalConfiguration;

        public Configurations()
        {
        }

        public Configurations(GlobalConfig gc, LocalConfig lc)
        {
            GobalConfiguration = gc;
            LocalConfiguration = lc;
        }
    }

    public partial class LocalConfig
    {
        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("mode")]
        public string Mode { get; set; }
    }

    public partial class LocalConfig
    {
        public static LocalConfig FromJson(string json) => JsonConvert.DeserializeObject<LocalConfig>(json, Converter.Settings);
    }

    public partial class GlobalConfig
    {
        [JsonProperty("database")]
        public DatabaseConfig DatabaseConfig { get; set; }

        public GlobalConfig()
        {
            DatabaseConfig = new DatabaseConfig();
        }
    }

    public partial class DatabaseConfig
    {
        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("dms_id")]
        public string DmsId { get; set; }

        [JsonProperty("connectionString")]
        public string ConnectionString { get; set; }
    }

    public partial class GlobalConfig
    {
        public static GlobalConfig FromJson(string json) => JsonConvert.DeserializeObject<GlobalConfig>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJsonGlobal(this GlobalConfig self) => JsonConvert.SerializeObject(self, Converter.Settings);
        public static string ToJsonLocal(this LocalConfig self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
