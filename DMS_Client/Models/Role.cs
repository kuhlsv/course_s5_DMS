﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.Models
{
    public class Role
    {
        public Role() { }
        public Role(string name)
        {
            this.Name = name;
        }
        public Role(int id, string name)
        {
            this.Name = name;
            this.Id = id;
        }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
