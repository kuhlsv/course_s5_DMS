﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.Models
{
    public class Document
    {
        private const int MIN_NUMBER = 100000000;
        private const int MAX_NUMBER = 999999999;

        public int DocId { get; set; }
        public string Name { get; set; }
        public string CryptedName { get; set; }
        public string Path { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string Signatur { get; set; }
        public int VersionNumber { get; set; }
        public bool IsArchieved { get; set; } = false;
        public double Size { get; set; }
        public User Owner { get; set; }

        public Document() { DocId = GenerateNumber(); }

        public Document(string name) { Name = name; DocId = GenerateNumber(); }

        public static int GenerateNumber()
        {
            Random rng = new Random();
            return rng.Next(MIN_NUMBER, MAX_NUMBER);
        }
    }
}
