﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.Models
{
    public class Fileplan
    {
        private const int MIN_NUMBER = 100000000;
        private const int MAX_NUMBER = 999999999;

        public int Filenumber { get; set; }
        public string Name { get; set; }
        public Fileplan Parent { get; set; }
        public DocumentCollection Documents { get; set; }
        public List<Role> Read { get; set; }
        public List<Role> ReadWrite { get; set; }

        public Fileplan()
        {
            Documents = new DocumentCollection();
            Filenumber = GenerateNumber();
        }

        public Fileplan(string name)
            : this()
        {
            Name = name;
        }

        public Fileplan(string name, Document doc)
            : this()
        {
            Documents.Add(doc);
            Name = name;
        }

        public static int GenerateNumber()
        {
            Random rng = new Random();
            return rng.Next(MIN_NUMBER, MAX_NUMBER);
        }
    }
}
