﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_Client.Models
{
    public class User
    {
        public User() {
            this.roles = new List<Role>();            
        }

        public User(string firstname, string lastname, string email, string password, List<Role> roles)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Email = email;
            this.Password = password;
            this.roles = roles;
        }

        private string firstname;
        public string FirstName
        {
            set { firstname = value; }
            get { return firstname; }
        }

        private string lastname;
        public string LastName
        {
            set { lastname = value; }
            get { return lastname; }
        }

        private string email;
        public string Email
        {
            set { email = value; }
            get { return email; }
        }

        private string password;
        public string Password
        {
            set { password = value; }
            get { return password; }
        }

        private List<Role> roles;
        public List<Role> Roles
        {
            set { roles = value; }
            get { return roles; }
        }

    }
}