﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

/// <summary>
/// Dev extend info: For tables add prefix + tables, for query fuction append comment
/// </summary>
namespace DMS_Client.Database
{

    public abstract class DB
    {
        // ConnectionString like "SERVER=localhost;DATABASE=test;UID=root;PASSWORD=test;"
        // or "Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=DMS;Integrated Security=True;"
        // similar to System.Configuration.ConfigurationManager.ConnectionStrings["LocalDB"].ConnectionString;
        private string connectionString;
        public SqlConnection Connection { get; set; }
        // Set a table prefix if needed
        private string prefix = "";
        public string Prefix { set => prefix = value + "."; }
        // Comment after querys for infos in log files
        public abstract string _comment { get; }

        public abstract bool InstallScript();

        public abstract bool UpdateScript();

        /// <summary>
        /// Constructor for develop mode
        /// </summary>
        /// <param name="conString">Define own connection string</param>
        public void SetConnection(string conString)
        {
            connectionString = conString ?? throw new ArgumentNullException("Argument cannot be null.");
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="host">Server IP</param>
        /// <param name="port">Server Port</param>
        /// <param name="database">Database Name</param>
        /// <param name="user">Autorisized user name</param>
        /// <param name="password">User password</param>
        public void SetConnection(string host, int port, string database, string user, string password)
        {
            if (host == null || database == null || user == null)
            {
                throw new ArgumentNullException("Arguments cannot be null.");
            }
            if(port <= 0)
            {
                port = 3306;
            }
            if (password == null)
            {
                password = "";
            }
            connectionString = "SERVER=" + host + "," + port + ";DATABASE=" + database + ";UID=" + user + ";PASSWORD=" + password + ";Connect Timeout=30;";
        }

        /// <summary>
        /// Open the connection to the database with the defined connection data
        /// </summary>
        /// <returns>Is connection opened</returns>
        public bool OpenConnection()
        {
            bool valid = false;
            if(connectionString != "")
            {
                Connection = new SqlConnection(connectionString);
                if (Connection != null)
                {
                    if (Connection.State == ConnectionState.Closed)
                    {
                        Connection.Open();
                        valid = true;
                    }
                }
            }
            return valid;
        }

        /// <summary>
        /// Close the open connection
        /// </summary>
        public void CloseConnection()
        {
            if (Connection != null)
            {
                if (Connection.State != ConnectionState.Closed)
                {
                    Connection.Close();
                }
            }
        }

        /// <summary>
        /// Close the Reader and the open connection
        /// Call this after selects (QueryReader())
        /// </summary>        
        /// <param name="reader">Reader to be closed before close connection</param>
        public void Close(SqlDataReader reader)
        {
            reader.Close();
            CloseConnection();
        }

        /// <summary>
        /// Create a table with a column definition string
        /// Use this in install script
        /// </summary>
        /// <param name="name">Table name</param>
        /// <param name="columnDefinitions">Define columns</param>
        /// <returns></returns>
        public bool CreateTable(string name, string columnDefinitions)
        {
            if (CheckForTable(name))
            {
                return true;
            }
            string query = "CREATE TABLE " + prefix + name + " ( " + columnDefinitions + " )";
            return QueryBool(query);
        }

        /// <summary>
        /// Select data
        /// Call Close(reader) after!
        /// </summary>
        /// <param name="column">Column to select</param>
        /// <param name="table">Table to select</param>
        /// <param name="where">Selected data identifier</param>
        /// <returns>Result data object</returns>
        public SqlDataReader Select(string column, string table, string where)
        {
            string query = "";
            if (where != "")
            {
               query = "SELECT " + column + " FROM " + prefix + table + " WHERE " + where + "";
            }else
            {
                query = "SELECT " + column + " FROM " + prefix + table;
            }
           
            return QueryReader(query);
        }

        public SqlDataReader Select(string table, string where)
        {
            return Select("*", table, where);
        }

        /// <summary>
        /// Update data
        /// </summary>
        /// <param name="table">Table to update</param>
        /// <param name="columnsvalues">Affected columns with Values</param>
        /// <param name="where">Selected data identifier</param>
        /// <returns>Are rows affected</returns>
        public bool Update(string table, string columnsvalues, string where)
        {
            string query = "UPDATE " + prefix + table + " SET " + columnsvalues + " WHERE " + where + "";
            return QueryBool(query);
        }

        /// <summary>
        /// Insert data
        /// </summary>
        /// <param name="table">Insert into table</param>
        /// <param name="columns">Columns</param>
        /// <param name="values">Values</param>
        /// <returns>Are rows affected</returns>
        public bool Insert(string table, string columns, string values)
        {
            string query = "INSERT INTO " + prefix + table + " ( " + columns + " ) VALUES (" + values + ")";
            return QueryBool(query);
        }

        public bool Insert(string table, string values)
        {
            return Insert(table, "*", values);
        }

        /// <summary>
        /// Delete data
        /// </summary>
        /// <param name="table">Delete from table</param>
        /// <param name="where">Selected data identifier</param>
        /// <returns>Are rows affected</returns>
        public bool Delete(string table, string where)
        {
            string query = "DELETE FROM " + prefix + table + " WHERE " + where + "";
            return QueryBool(query);
        }

        /// <summary>
        /// Query data with full/custom query string
        /// Do not to call Close()
        /// </summary>
        /// <param name="query">Query string</param>
        /// <returns>Are rows affected</returns>
        public bool QueryBool(string query)
        {
            bool valid = false;
            try
            {
                if (OpenConnection())
                {
                    SqlCommand cmd = new SqlCommand(query + " " + _comment, Connection);
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        valid = true;
                    }
                    CloseConnection();
                }
            }
            catch (Exception e)
            {
                Console.Write("Can not write to database!");
            }
            return valid;
        }

        /// <summary>
        /// Query data with full/custom query string
        /// Call Close() after this
        /// </summary>
        /// <param name="query">Query string</param>
        /// <returns>Result data object, can be null</returns>
        public SqlDataReader QueryReader(string query)
        {
            if (OpenConnection())
            {
                SqlCommand cmd = new SqlCommand(query + " " + _comment, Connection);
                return cmd.ExecuteReader();
            }
            else
            {
                return null;
            }
        }

        public bool CheckForTable(string tablename)
        {
            bool exists = false;
            try
            {
                // SQL DEV test
                exists = QueryBool("select * from " + tablename + "");
            }
            catch
            {
                try
                {
                    exists = true;
                    var cmdOthers = new OdbcCommand("select 1 from " + tablename + " where 1 = 0");
                    cmdOthers.ExecuteNonQuery();
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }
    }
}