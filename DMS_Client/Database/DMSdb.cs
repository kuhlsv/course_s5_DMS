﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMS_Client.Models;

namespace DMS_Client.Database
{
    public class DMSdb : DB
    {
        /*
         * Properties
         * */
        public override string _comment => "/* DMS Client Software */"; // Info for logging stystems, appended on querys
        private const string ADMIN_MAIL = "admin@dms.de";
        private const string ADMIN_PW = "admin";

        /*
         * Singleton
         * First run SetConnection();
         * */
        private static DMSdb instance = null;
        public static DMSdb Instance
        { 
            get
            {
                if (instance == null)
                {
                    instance = new DMSdb();
                }
                return instance;
            }
        }

        /*
         * Init
         * */

        public override bool InstallScript()
        {
            try
            {
                string userTable = "Users";
                string userColumns = "FirstName VARCHAR(60), LastName VARCHAR(60), " +
                    "Password VARCHAR(100), Email VARCHAR(60), PRIMARY KEY (Email)"; // TODO CREATE ALL RELATION TABLES (ALSO QUERYS)
                CreateTable(userTable, userColumns);
                string roleTable = "Roles";
                string roleColumns = "Id INT IDENTITY(1,1), Name VARCHAR(60), PRIMARY KEY (Id)";
                CreateTable(roleTable, roleColumns);
                string userRoleTable = "UsersRoles";
                string userRoleColumns = "UserEmail VARCHAR(60) FOREIGN KEY REFERENCES Users(Email), RoleId INT FOREIGN KEY REFERENCES Roles(Id)" +
                           ", PRIMARY KEY (UserEmail, RoleId)";
                CreateTable(userRoleTable, userRoleColumns);
                string documentTable = "Documents";
                string documnetColumns = "DocId INT, Name VARCHAR(100), CryptedName VARCHAR(180), Path VARCHAR(180), " +
                        "Type VARCHAR(60), Category VARCHAR(60), Signatur VARCHAR(180), Version INT, " +
                        "IsArchived BIT, Size FLOAT(32), Owner VARCHAR(60), PRIMARY KEY (DocId)";
                CreateTable(documentTable, documnetColumns); // Version is no Primary key till it is not implemented
                string fileplanTable = "Fileplan";
                string fileplanColumns = "Filenumber INT, Name VARCHAR(60), PRIMARY KEY (Filenumber)";
                CreateTable(fileplanTable, fileplanColumns);
                string fileplanDocumentsTable = "FileplanDocuments";
                string fileplanDocumentsColumns = "Filenumber INT FOREIGN KEY REFERENCES Fileplan(Filenumber), DocId INT FOREIGN KEY REFERENCES Documents(DocId)" +
                           ", PRIMARY KEY (Filenumber, DocId)";
                CreateTable(fileplanDocumentsTable, fileplanDocumentsColumns);
                string settingsTable = "Settings";
                string settingsColumns = "Id INT IDENTITY(1,1), Name VARCHAR(60), Value VARCHAR(60), PRIMARY KEY (Id)";
                CreateTable(settingsTable, settingsColumns);

                // Initial Role
                if (SelectRole(1).Id == 0)
                {
                    InsertRole(new Role("Admin"));
                }
                //Initial Admin User
                User user = SelectUser(ADMIN_MAIL);
                if (user.Email == null)
                {
                    List<Role> roles = new List<Role>();
                    roles.Add(new Role(1, "Admin"));
                    InsertUser(new User("Admin", "Admin", ADMIN_MAIL, ADMIN_PW, roles));
                }
            }
            catch (Exception e)
            {
                Console.Write("Warning: Tables already exists");
            }
            return true;
        }

        public override bool UpdateScript()
        {
            throw new NotImplementedException();
        }

        /******************************************************************
         * Object Querys *
         * ***************************************************************/

        /*
         * User
         * */

       public UserCollection SelectAllUsers()
        {
            UserCollection uc = new UserCollection();

            SqlDataReader reader = Select("Users", "");
            if (reader != null)
            {
                while (reader.Read())
                {
                    User user = new User();
                    user.FirstName = reader["FirstName"].ToString();
                    user.LastName = reader["LastName"].ToString();
                    user.Email = reader["Email"].ToString();
                    user.Password = reader["Password"].ToString();
                    List<Role> roles = SelectUserRoles(user);
                    user.Roles = roles;
                    uc.Add(user);
                }
                Close(reader);

            }
            return uc;
        }
 

        public User SelectUser(string email)
        {
            SqlDataReader reader = Select("Users", "Email = '" + email + "'");
            User user = new User();
            if (reader == null)
            {
                return user;
            }
            while (reader.Read())
            {
                user.FirstName = reader["FirstName"].ToString();
                user.LastName = reader["LastName"].ToString();
                user.Email = reader["Email"].ToString();
                user.Password = reader["Password"].ToString();
                
            }
            Close(reader);

            List<Role> roles = SelectUserRoles(user);
            user.Roles = roles; 
            return user;
        }

        public bool InsertUser(User user)
        {
            string columns = "FirstName, LastName, Email, Password";
            string values = "'" + user.FirstName + "', '" + user.LastName + "', '" + user.Email + "', '" + user.Password + "'";
            Insert("Users", columns, values);
            return InsertUserRoles(user);
        }

        public bool UpdateUser(User user)
        {
            string columnsvalues = "FirstName = '" + user.FirstName + "', LastName = '" + user.LastName + "', Email = '"
                                    + user.Email + "', Password = '" + user.Password + "'";
            DeleteUserRoles(user);
            InsertUserRoles(user);
            return Update("Users", columnsvalues, "Email = '" + user.Email + "'");
        }

        public bool DeleteUser(User user)
        {
            return Delete("Users", "Email = '" + user.Email + "'");
        }

        /*
         * Document
         * */

        public List<Document> SelectAllDocuments()
        {
            List<Document> docs = new List<Document>();
            SqlDataReader reader = Select("Documents", "");
            if (reader != null)
            {
                while (reader.Read())
                {
                    Document doc = new Document();
                    int id = 0;
                    int.TryParse(reader["DocId"].ToString(), out id);
                    doc.DocId = id;
                    doc.Name = reader["Name"].ToString();
                    doc.CryptedName = reader["CryptedName"].ToString();
                    doc.Path = reader["Path"].ToString();
                    doc.Type = reader["Type"].ToString();
                    doc.Category = reader["Category"].ToString();
                    doc.Signatur = reader["Signatur"].ToString();
                    int.TryParse(reader["Version"].ToString(), out int versionNumber);
                    doc.VersionNumber = versionNumber;
                    bool.TryParse(reader["IsArchived"].ToString(), out bool archived);
                    doc.IsArchieved = archived;
                    double.TryParse(reader["Size"].ToString(), out double size);
                    doc.Size = size;
                    doc.Owner = this.SelectUser(reader["Owner"].ToString());
                    docs.Add(doc);
                }
            }
            Close(reader);
            return docs;
        }

        public Document SelectDocument(int docId, int version)
        {
            SqlDataReader reader = Select("Documents", "DocId = " + docId + " AND Version = " + version +"");
            Document doc = new Document();
            doc.DocId = 0;
            if (reader == null)
            {
                return doc;
            }
            while (reader.Read())
            {
                int id = 0;
                int.TryParse(reader["DocId"].ToString(), out id);
                doc.DocId = id;
                doc.Name = reader["Name"].ToString();
                doc.CryptedName = reader["CryptedName"].ToString();
                doc.Path = reader["Path"].ToString();
                doc.Type = reader["Type"].ToString();
                doc.Category = reader["Category"].ToString();
                doc.Signatur = reader["Signatur"].ToString();
                int.TryParse(reader["Version"].ToString(), out int versionNumber);
                doc.VersionNumber = versionNumber;
                bool.TryParse(reader["IsArchived"].ToString(), out bool archived);
                doc.IsArchieved = archived;
                double.TryParse(reader["Size"].ToString(), out double size);
                doc.Size = size;
                doc.Owner = this.SelectUser(reader["Owner"].ToString());
            }
            Close(reader);
            return doc;
        }

        public bool UpdateDocument(Document doc)
        {
            string columnsvalues = "DocId = " + doc.DocId + ", Name = '" + doc.Name + "', CryptedName ='" + doc.CryptedName + "', Path = '" + doc.Path + "', Type = '" + doc.Type + "', Category = '" + doc.Category + "', Signatur = '" + doc.Signatur + "', Version = " + doc.VersionNumber + ", IsArchived = " + (doc.IsArchieved ? 1 : 0).ToString() + ", Size = " + (float)doc.Size + ", Owner = '" + doc.Owner.Email + "'";
             return Update("Documents", columnsvalues, "DocId = " + doc.DocId + " AND Version = " + doc.VersionNumber + "");
        }

        public bool InsertDocument(Document doc)
        {
            string columns = "DocId, Name, CryptedName, Path, Type, Category, Signatur, Version, IsArchived, Size, Owner";
            string values = "" + doc.DocId + ", '" + doc.Name + "', '" + doc.CryptedName + "', '" + doc.Path + "', '" + doc.Type + "', '" + doc.Category + "', '" + doc.Signatur + "', " + doc.VersionNumber + ", '" + (doc.IsArchieved ? 1 : 0).ToString() + "', " + (float)doc.Size + ", '" + doc.Owner.Email + "'";
            return Insert("Documents", columns, values);
        }

        public bool DeleteDocument(Document doc)
        {
            DeleteFileplanDocuments(doc);
            return Delete("Documents", "DocId = " + doc.DocId + " AND Version = " + doc.VersionNumber + "");
        }

        /*
        * Fileplan
        * */

        public List<Fileplan> SelectAllFileplan()
        {
            List<Fileplan> fps = new List<Fileplan>();
            SqlDataReader reader = Select("Fileplan", "");
            if (reader != null)
            {
                while (reader.Read())
                {
                    Fileplan fp = new Fileplan();
                    fp.Name = reader["Name"].ToString();
                    int id = 0;
                    int.TryParse(reader["FileNumber"].ToString(), out id);
                    fp.Filenumber = id;
                    foreach (int i in SelectFileplanDocuments(fp.Filenumber))
                    {
                        fp.Documents.Add(SelectDocument(i, 1));
                    }
                    fps.Add(fp);
                }
                Close(reader);

            }
            return fps;
        }

        public Fileplan SelectFileplan(int fileNumber)
        {
            SqlDataReader reader = Select("Fileplan", "FileNumber = " + fileNumber + "");
            Fileplan fp = new Fileplan();
            if (reader == null)
            {
                return fp; 
            }
            while (reader.Read())
            {
                int id = fileNumber;
                int.TryParse(reader["FileNumber"].ToString(), out id);
                fp.Filenumber = id;
                fp.Name = reader["Name"].ToString();
                fp.Parent = null; // TODO GET PARENTS
                fp.Read = null;
                fp.ReadWrite = null;
                foreach(int i in SelectFileplanDocuments(fp.Filenumber))
                {
                    fp.Documents.Add(SelectDocument(i, 1));
                }
            }
            Close(reader);
            return fp;
        }

        public bool UpdateFileplan(Fileplan fp)
        {
            string columnsvalues = "FileNumber = " + fp.Filenumber + ", Name = '" + fp.Name + "'";
            UpdateFileplanDocuments(fp);
            return Update("Fileplan", columnsvalues, "FileNumber = " + fp.Filenumber + "");
        }

        public bool InsertFileplan(Fileplan fp)
        {
            string columns = "FileNumber, Name";
            string values = fp.Filenumber + ", '" + fp.Name + "'";
            InsertFileplanDocuments(fp);
            return Insert("Fileplan", columns, values);
        }

        public bool DeleteFileplan(Fileplan fp)
        {
            return Delete("Fileplan", "FileNumber = " + fp.Filenumber + "");
        }

        /*
        * Role
        * */
        public RoleCollection SelectAllRoles()
        {
           RoleCollection rc = new RoleCollection();

            SqlDataReader reader = Select("Roles", "");
            if (reader != null)
            {
                while (reader.Read())
                {
                    Role role = new Role();
                    role.Name = reader["Name"].ToString();
                    role.Id = ((int)reader["Id"]);
                    rc.Add(role);
                }
                Close(reader);

            }
            return rc;
        }

        public Role SelectRole(int rid)
        {
            SqlDataReader reader = Select("Roles", "Id = '" + rid + "'");
            Role role = new Role();
            if (reader == null)
            {
                return role;
            }
            while (reader.Read())
            {
                int id = rid;
                int.TryParse(reader["Id"].ToString(), out id);
                role.Id = id;
                role.Name = reader["Name"].ToString();
            }
            Close(reader);
            return role;
        }

        public bool UpdateRole(Role role)
        {
            string columnsvalues = "Id = " + role.Id + ", Name = '" + role.Name + "'";
            return Update("Roles", columnsvalues, "Id = " + role.Id + "");
        }

        public bool InsertRole(Role role)
        {
            string columns = "Name";
            string values = "'" + role.Name + "'";
            return Insert("Roles", columns, values);
        }

        public bool DeleteRole(Role role)
        {
            return Delete("Roles", "Id = " + role.Id + "");
        }

        /*
         * User Role
         */

         public bool InsertUserRoles(User user)
        {
            bool success = true;
            string columns = "UserEmail, RoleId";
            foreach (Role role in user.Roles)
            {
                string values = "'" + user.Email + "', " + role.Id;
                success = Insert("UsersRoles", columns, values);
            }

            return success;
        }

        public List<Role> SelectUserRoles(User user)
        {
            List<Role> roles = new List<Role>();
            SqlDataReader reader = Select("UsersRoles", "UserEmail='" + user.Email +"'");
            if (reader != null)
            {
                while (reader.Read())
                {
                    int roleId = ((int)reader["RoleId"]);
                    Role role = SelectRole(roleId);
                    roles.Add(role);
                }
                Close(reader);
            }
            return roles;
        }

        public bool DeleteUserRoles(User user)
        {
            return Delete("UsersRoles", "UserEmail = '" + user.Email + "'");
        }

        /*
        * Fileplan Documents
        */

        public int[] SelectFileplanDocuments(int filenumber)
        {
            SqlDataReader reader = Select("*", "FileplanDocuments", "Filenumber = " + filenumber);
            List<int> docs = new List<int>();
            if (reader == null)
            {
                return docs.ToArray();
            }
            while (reader.Read())
            {
                int.TryParse(reader["Filenumber"].ToString(), out int number);
                if(number == filenumber)
                {
                    int.TryParse(reader["DocId"].ToString(), out int doc);
                    docs.Add(doc);
                }
            }
            Close(reader);
            return docs.ToArray();
        }

        public bool InsertFileplanDocuments(Fileplan fp)
        {
            bool success = true;
            string columns = "Filenumber, DocId";
            foreach (Document doc in fp.Documents)
            {
                string values = "" + fp.Filenumber + ", " + doc.DocId;
                success = Insert("FileplanDocuments", columns, values);
            }

            return success;
        }

        public bool UpdateFileplanDocuments(Fileplan fp)
        {
            bool success = true;
            string columns = "Filenumber, DocId";
            foreach (Document doc in fp.Documents)
            {
                if (!QueryBool("SELECT * FROM FileplanDocuments WHERE Filenumber = "+fp.Filenumber+ " AND DocId = " + doc.DocId))
                {
                    string values = "" + fp.Filenumber + ", " + doc.DocId;
                    success = Insert("FileplanDocuments", columns, values);
                }
            }
            return success;
        }

        public bool DeleteFileplanDocuments(Document doc)
        {
            return Delete("FileplanDocuments", "DocId = '" + doc.DocId + "'");
        }

    }
}
