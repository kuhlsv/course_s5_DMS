﻿using DMS_Client.ViewModels;
using System;
using System.Windows;

namespace DMS_Client
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            // Setup
            SetupViewModel svm = new SetupViewModel();
        }
    }
}
