# DMS-SE2

<h2>Aufruf</h2>

App.xml -> Wenn Config.json gelesen werden kann und Datenbankverbindung steht -> Login.xml<br>
App.xml -> Wenn KEINE Config.json gelesen werden kann -> Setup.xml -> Login.xml<br>
Datenbank und Logger müssen in der App.xaml.cs initialisiert werden


<h2>Config</h2>

Es gibt im Verzeichnis des Pogramms eine Config.json die den <i>Mode</i> (Develop/Productive) und <i>Pfad</i> festlegt.<br>
Der Mode hat Einfluss auf die Datenbankanbindung. Pfad speichert den Pfad zum Netzwerklaufwerk und der damit verbundenen <i>Metadata.dms</i>. So kann das merken des Pfades beim Beenden des Programmes gewährleistet werden.<br>


<h2>Laufwerk</h2>

Das Laufwerk wird duch eine config definiert. Diese <i>Metadata.dms</i> hält die Datenbankanbindung zu dem Laufwerk und eine Identifikationsnummer, damit Datenbank und zugehöriges Laufwerk zusammenarbeiten können.<br>


<h2>Database</h2>

Die Datenbank hat zwei Modi: Develop / Production
Diese sind in der Config.json definiert.
<ul>
<li>
<b>Develop:</b> In diesem Modus nutzt die Software eine Datenbankverbindung die Local (LocalDB) von Visual Studio aus genutzt werden kann. 
Dies fungiert über ein connection-string der Hardcoded ist. Über den SQL-Server-Explorer kann die DB angelegt werden. DB-Name: DMS
</li>
<li>
<b>Productive:</b> In diesem Modus nutzt die SOftware eine Datenbankverbindung zu einem externen Server, die über das Setup-Fenster beim ersten start eigegeben wird.
</li>
</ul>
Die Datenbank ist ein Singleton und kann über DMSdb.Instance genutzt werden. Sie erbt von der eigenen Klasse DB.

<h2>Logs</h2>

Logs können über die statischen Methoden Log.C (Console), Log.F (File), Log.D (Database) aufgerufen werden.<br>
Sollen weiter werte initialisiert werden, muss statt den statischen Methden der Logger initialisiert werden.<br> 
Hier kann die Datenbankanbindung als Instanz und der Dateipfad festgelegt werden (Loggger.Type = Logger.LogType.Database).<br>
Anschließend reicht ein Logger.Write() zum loggen.

<h2>NUnit Test</h2>
Für die NUnit Tests wird eine Datenbank mit dem Namen DMS_Test benötigt.